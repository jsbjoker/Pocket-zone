﻿using System;

namespace Common.Mediators.Quit.Interfaces
{
    public interface IQuitView
    {
        event Action OnGameExit;
    }
}