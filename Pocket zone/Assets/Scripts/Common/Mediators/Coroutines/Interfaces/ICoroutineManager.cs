using System.Collections;
using UnityEngine;

namespace Common.Mediators.Coroutines.Interfaces
{
    public interface ICoroutineManager
    {
        Coroutine StartCoroutine(IEnumerator coroutine);
        void StopCoroutine(IEnumerator coroutine);
        void StopCoroutine(Coroutine coroutine);
    }
}