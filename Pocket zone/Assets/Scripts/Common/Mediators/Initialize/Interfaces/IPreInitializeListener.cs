﻿using Common.Mediators.Initialize.Enums;

namespace Common.Mediators.Initialize.Interfaces
{
    public interface IPreInitializeListener
    {
        EInitializeOrder InitializeOrder { get; }
        void Initialize();
    }
}