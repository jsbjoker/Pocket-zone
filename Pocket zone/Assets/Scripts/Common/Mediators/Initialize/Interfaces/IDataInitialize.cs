﻿namespace Common.Mediators.Initialize.Interfaces
{
    public interface IDataInitialize
    {
        void SetupData();
    }
}