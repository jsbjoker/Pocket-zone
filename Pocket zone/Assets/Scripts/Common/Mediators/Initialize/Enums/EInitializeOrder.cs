﻿namespace Common.Mediators.Initialize.Enums
{
    public enum EInitializeOrder
    {
        First,
        Second,
        Third,
        Fourth
    }
}