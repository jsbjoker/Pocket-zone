﻿namespace Common.Mediators.Pause.Interfaces
{
    public interface IPauseListener
    {
        void OnApplicationPause(bool pauseStatus);
    }
}