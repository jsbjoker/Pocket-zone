﻿using System;

namespace Common.Mediators.Pause.Interfaces
{
    public interface IPauseView
    {
        event Action<bool> OnGamePaused;
    }
}