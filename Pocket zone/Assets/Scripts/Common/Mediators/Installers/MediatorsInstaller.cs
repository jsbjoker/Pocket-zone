﻿using Common.Mediators.Initialize.Controllers;
using Common.Mediators.Pause.Controllers;
using Common.Mediators.Quit.Controllers;
using Common.Mediators.Update.Controllers;
using Common.Mediators.Views;
using UnityEngine;
using Zenject;

namespace Common.Mediators.Installers
{
    [CreateAssetMenu(fileName = "MediatorsInstaller", menuName = "Installers/Common/MediatorsInstaller")]
    public class MediatorsInstaller : ScriptableObjectInstaller
    {
        [SerializeField] private MediatorView _mediatorView;
        
        public override void InstallBindings()
        {
            Container.BindInterfacesTo<MediatorView>().FromComponentInNewPrefab(_mediatorView).AsSingle();
            Container.BindInterfacesTo<InitializeMediator>().AsSingle();
            Container.BindInterfacesTo<PauseMediator>().AsSingle();
            Container.BindInterfacesTo<QuitMediator>().AsSingle();
            Container.BindInterfacesTo<UpdateMediator>().AsSingle();
        }
    }
}