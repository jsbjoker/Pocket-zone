using UnityEngine;

[ExecuteInEditMode]
public class SafeAreaView : MonoBehaviour
{
    private RectTransform _panel;
    private Rect _lastSafeArea = new Rect (0, 0, 0, 0);

    private void Awake ()
    {
        _panel = GetComponent<RectTransform> ();
        Refresh ();
    }

    private void Update ()
    {
        Refresh ();
    }

    private void Refresh ()
    {
        var safeArea = SafeArea;

        if (safeArea != _lastSafeArea)
            ApplySafeArea (safeArea);
    }

    private Rect SafeArea => Screen.safeArea;

    private void ApplySafeArea (Rect safeArea)
    {
        _lastSafeArea = safeArea;

        //Convert safe area rectangle from absolute pixels to normalised anchor coordinates
        var anchorMin = safeArea.position;
        var anchorMax = safeArea.position + safeArea.size;
        anchorMin.x /= Screen.width;
        anchorMin.y /= Screen.height;
        //anchorMin.y = 0;
        anchorMax.x /= Screen.width;
        anchorMax.y /= Screen.height;
        _panel.anchorMin = anchorMin;
        _panel.anchorMax = anchorMax;
    }
}

