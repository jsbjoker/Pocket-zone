﻿using Common.DataStorage.SaveBehaviours.Controllers;
using Common.DataStorage.Storage.Controllers;
using UnityEngine;
using Zenject;

namespace Common.DataStorage.Installers
{
    [CreateAssetMenu(fileName = "DataStorageInstaller", menuName = "Installers/Common/DataStorageInstaller")]
    public class DataStorageInstaller : ScriptableObjectInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesTo<DataStorageController>().AsSingle();
            Container.BindInterfacesTo<PrefsSaveBehaviour>().AsSingle();
        }
    }
}