﻿using System;

namespace Common.DataStorage.Storage.Interfaces
{
    public interface IDataStorage
    {
        string Key { get; }
        Type DataType { get; }
        
        event Action<IDataStorage> OnChange;

        object GetData();
        void SetData(object dataObject);
    }
}