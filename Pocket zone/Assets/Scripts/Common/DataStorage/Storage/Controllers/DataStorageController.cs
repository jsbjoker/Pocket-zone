﻿using System;
using System.Collections.Generic;
using Common.DataStorage.SaveBehaviours.Interfaces;
using Common.DataStorage.Storage.Interfaces;

namespace Common.DataStorage.Storage.Controllers
{
    public class DataStorageController : IDataStorageController, IDisposable
    {
        private readonly IDataSaveBehaviour _dataSaveBehaviour;
        private readonly List<IDataStorage> _dataStorages;

        public DataStorageController(IDataSaveBehaviour dataSaveBehaviour, List<IDataStorage> dataStorages)
        {
            _dataSaveBehaviour = dataSaveBehaviour;
            _dataStorages = dataStorages;
            Initialize();
        }

        private void Initialize()
        {
            foreach (var dataStorage in _dataStorages)
            {
                Load(dataStorage);
                dataStorage.OnChange += Save;
            }
        }
        
        private void Load(IDataStorage dataStorage)
        {
            var key = dataStorage.Key;
            var loadData = _dataSaveBehaviour.GetObject(key, dataStorage.DataType);
            dataStorage.SetData(loadData);
        }

        private void Save(IDataStorage dataStorage)
        {
            var key = dataStorage.Key;
            var data = dataStorage.GetData();
            _dataSaveBehaviour.SetObject(key, data);
        }

        public void Dispose()
        {
            foreach (var dataStorage in _dataStorages)
            {
                dataStorage.OnChange -= Save;
            }
        }
    }
}