﻿using System;

namespace Common.DataStorage.SaveBehaviours.Interfaces
{
    public interface IDataSaveBehaviour
    {
        object GetObject(string key, Type type);
        void SetObject(string key, object obj);
    }
}