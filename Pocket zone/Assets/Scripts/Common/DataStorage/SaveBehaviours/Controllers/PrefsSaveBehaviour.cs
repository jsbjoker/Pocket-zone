﻿using System;
using Common.DataStorage.SaveBehaviours.Interfaces;
using Newtonsoft.Json;
using UnityEngine;

namespace Common.DataStorage.SaveBehaviours.Controllers
{
    public class PrefsSaveBehaviour : IDataSaveBehaviour
    {
        public object GetObject(string key, Type type)
        {
            var prefsObject = PlayerPrefs.GetString(key);
            return string.IsNullOrEmpty(prefsObject) ? null : JsonConvert.DeserializeObject(prefsObject, type);
        }

        public void SetObject(string key, object obj)
        {
            var serializeObject = JsonConvert.SerializeObject(obj);
            PlayerPrefs.SetString(key, serializeObject);
        }
    }
}