﻿using Common.TextSerializer.Controllers;
using UnityEngine;
using Zenject;

namespace Common.TextSerializer.Installers
{
    [CreateAssetMenu(fileName = "TextSerializerInstaller", menuName = "Installers/Common/TextSerializerInstaller")]
    public class TextSerializerInstaller : ScriptableObjectInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesTo<TextSerializerController>().AsSingle();
        }
    }
}