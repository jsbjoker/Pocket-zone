﻿namespace Common.TextSerializer.Interfaces
{
    public interface ITextSerializer
    {
        bool Serialize(object obj, out string serializeData);
        bool Deserialize<T>(string text, out T config);
    }
}