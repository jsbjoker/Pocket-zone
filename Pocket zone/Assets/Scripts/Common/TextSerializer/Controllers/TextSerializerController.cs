﻿using Common.TextSerializer.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Common.TextSerializer.Controllers
{
    public class TextSerializerController : ITextSerializer
    {
        public TextSerializerController()
        {
            JsonConvert.DefaultSettings = () =>
            {
                var settings = new JsonSerializerSettings();
                settings.Converters.Add(new StringEnumConverter {CamelCaseText = false});
                return settings;
            };
        }
        
        public bool Serialize(object obj, out string serializeData)
        {
            var dontHasErrors = true;
            serializeData = JsonConvert.SerializeObject(obj, new JsonSerializerSettings()
            {
                Error = delegate(object sender, ErrorEventArgs args) { dontHasErrors = false; }
            });
            return dontHasErrors;
        }

        public bool Deserialize<T>(string text, out T config)
        {
            var dontHasErrors = true;
            config = JsonConvert.DeserializeObject<T>(text, new JsonSerializerSettings()
            {
                Error = delegate(object sender, ErrorEventArgs args) { dontHasErrors = false; }
            });
            return dontHasErrors;
        }
    }
}