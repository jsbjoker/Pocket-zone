using Game.Inventories.Views;

namespace Game.Inventories.Interfaces
{
    public interface ISlotFactory
    {
        void CreateSlot(int slotID, SlotView slotView);
    }
}