using System;
using Game.Items.Interfaces;

namespace Game.Inventories.Interfaces
{
    public interface ISlot : IDisposable
    {
        int SlotID { get; }
        bool ContainItem { get; }
        IItem SlotItem { get; }
        int ItemAmount { get; }
        void SetSlotItem(IItem item);
        void AddItemAmount(int itemAmount);
        void RemoveItemFromSlot();
    }
}