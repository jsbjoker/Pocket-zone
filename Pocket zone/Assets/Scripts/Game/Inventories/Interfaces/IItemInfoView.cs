using System;
using UnityEngine;

namespace Game.Inventories.Interfaces
{
    public interface IItemInfoView
    {
        void SetItemIcon(Sprite sprite);
        void SetItemAmount(string amount);
        event Action OnCloseClick;
        event Action OnRemoveClick;
    }
}