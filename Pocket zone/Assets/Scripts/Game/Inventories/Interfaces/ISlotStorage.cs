using System.Collections.Generic;
using Game.Items.Enums;
using Game.Items.Interfaces;

namespace Game.Inventories.Interfaces
{
    public interface ISlotStorage
    {
        IReadOnlyDictionary<int, ISlot> Slots { get; }
        ISlot this[int slotID] { get; }
        void AddSlot(ISlot slot);
        ISlot GetFirstEmptySlot();
        ISlot GetSlotWithItemType(EItem itemType);
        ISlot GetSlotWithItem(IItem item);
    }
}