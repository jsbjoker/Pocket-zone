using Game.Items.Interfaces;

namespace Game.Inventories.Interfaces
{
    public interface IItemInfoPresenter
    {
        void ShowItemInfo(IItem item);
    }
}