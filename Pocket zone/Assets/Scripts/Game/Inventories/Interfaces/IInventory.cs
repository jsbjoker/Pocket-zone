using Game.Items.Enums;
using Game.Items.Interfaces;

namespace Game.Inventories.Interfaces
{
    public interface IInventory
    {
        void AddItem(EItem itemType);
        void AddItem(EItem itemType, int amount);
        void RemoveItem(IItem item);
    }
}