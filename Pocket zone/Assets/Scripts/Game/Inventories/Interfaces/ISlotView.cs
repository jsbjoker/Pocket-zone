using System;
using UnityEngine;

namespace Game.Inventories.Interfaces
{
    public interface ISlotView
    {
        void SetItemImageActiveState(bool value);
        void SetItemImage(Sprite sprite);
        void SetItemAmount(string amount);
        event Action OnSlotClick;
    }
}