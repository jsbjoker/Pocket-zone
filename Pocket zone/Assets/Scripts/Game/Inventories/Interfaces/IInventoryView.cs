using System;
using System.Collections.Generic;
using Game.Inventories.Views;

namespace Game.Inventories.Interfaces
{
    public interface IInventoryView
    {
        IReadOnlyList<SlotView> SlotViews { get; }
        event Action OnInventoryClose;
        event Action OnInventoryOpen;
    }
}