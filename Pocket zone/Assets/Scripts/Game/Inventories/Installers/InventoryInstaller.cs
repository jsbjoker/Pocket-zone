using Game.Inventories.Controllers;
using Game.Inventories.Views;
using UnityEngine;
using Zenject;

namespace Game.Inventories.Installers
{
    public class InventoryInstaller : MonoInstaller
    {
        [SerializeField] private InventoryView _inventoryView;
        [SerializeField] private ItemInfoView _itemInfoView;
        
        public override void InstallBindings()
        {
            Container.BindInterfacesTo<ItemInfoView>().FromInstance(_itemInfoView).AsSingle();
            Container.BindInterfacesTo<InventoryView>().FromInstance(_inventoryView).AsSingle();
            Container.BindInterfacesTo<SlotFactory>().AsSingle();
            Container.BindInterfacesTo<SlotInitializer>().AsSingle();
            Container.BindInterfacesTo<SlotStorage>().AsSingle();
            Container.BindInterfacesTo<Inventory>().AsSingle();
            Container.BindInterfacesTo<InventoryPresenter>().AsSingle();
            Container.BindInterfacesTo<ItemInfoPresenter>().AsSingle();
        }
    }
}