using System;
using Game.Inventories.Interfaces;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Inventories.Views
{
    public class SlotView : MonoBehaviour, ISlotView
    {
        [SerializeField] private Image _itemImage;
        [SerializeField] private TextMeshProUGUI _itemAmount;
        [SerializeField] private Button _slotButton;
        
        public event Action OnSlotClick;

        private void Awake()
        {
            _slotButton.onClick.AddListener(() =>
            {
                OnSlotClick?.Invoke();
            });
        }

        public void SetItemImageActiveState(bool value) => _itemImage.gameObject.SetActive(value);

        public void SetItemImage(Sprite sprite) => _itemImage.sprite = sprite;

        public void SetItemAmount(string amount) => _itemAmount.text = amount;

        private void OnDestroy()
        {
            _slotButton.onClick.RemoveAllListeners();
            OnSlotClick = null;
        }
    }
}