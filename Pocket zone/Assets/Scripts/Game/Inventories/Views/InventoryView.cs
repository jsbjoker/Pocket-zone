using System;
using System.Collections.Generic;
using Game.Inventories.Interfaces;
using Game.UI.Enums;
using Game.UI.Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Inventories.Views
{
    public class InventoryView : MonoBehaviour, IInventoryView, IWindowView
    {
        [SerializeField] private EWindow _window;
        [SerializeField] private List<SlotView> _slotViewViews;
        [SerializeField] private Button _openInventoryButton;
        [SerializeField] private Button _closeInventoryButton;
        
        public event Action OnInventoryOpen;
        public event Action OnInventoryClose;
        public IReadOnlyList<SlotView> SlotViews => _slotViewViews;
        public EWindow Window => _window;

        private void Awake()
        {
            _openInventoryButton.onClick.AddListener(() =>
            {
                OnInventoryOpen?.Invoke();
            });
            _closeInventoryButton.onClick.AddListener(() =>
            {
                OnInventoryClose?.Invoke();
            });
        }
        
        public void SetWindowActiveState(bool value) => gameObject.SetActive(value);

        private void OnDestroy()
        {
            _openInventoryButton.onClick.RemoveAllListeners();
            _closeInventoryButton.onClick.RemoveAllListeners();
            OnInventoryOpen = null;
            OnInventoryClose = null;
        }
    }
}