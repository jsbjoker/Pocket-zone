using System;
using Game.Inventories.Interfaces;
using Game.UI.Enums;
using Game.UI.Interfaces;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Inventories.Views
{
    public class ItemInfoView : MonoBehaviour, IItemInfoView, IWindowView
    {
        [SerializeField] private EWindow _window;
        [SerializeField] private Image _itemIcon;
        [SerializeField] private TextMeshProUGUI _itemAmount;
        [SerializeField] private Button _closeButton;
        [SerializeField] private Button _removeButton;
        
        public event Action OnCloseClick;
        public event Action OnRemoveClick;
        
        public EWindow Window => _window;

        private void Awake()
        {
            _closeButton.onClick.AddListener(() =>
            {
                OnCloseClick?.Invoke();
            });
            _removeButton.onClick.AddListener(() =>
            {
                OnRemoveClick?.Invoke();
            });
        }

        public void SetItemIcon(Sprite sprite) => _itemIcon.sprite = sprite;

        public void SetItemAmount(string amount) => _itemAmount.text = amount;
        public void SetWindowActiveState(bool value) => gameObject.SetActive(value);

        private void OnDestroy()
        {
            OnCloseClick = null;
            OnRemoveClick = null;
            _closeButton.onClick.RemoveAllListeners();
            _removeButton.onClick.RemoveAllListeners();
        }
    }
}