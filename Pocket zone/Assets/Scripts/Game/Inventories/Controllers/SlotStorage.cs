using System;
using System.Collections.Generic;
using System.Linq;
using Game.Inventories.Interfaces;
using Game.Items.Enums;
using Game.Items.Interfaces;

namespace Game.Inventories.Controllers
{
    public class SlotStorage : ISlotStorage
    {
        private readonly Dictionary<int, ISlot> _slots = new();

        public IReadOnlyDictionary<int, ISlot> Slots => _slots;

        public ISlot this[int slotID] => _slots.ContainsKey(slotID)
            ? _slots[slotID]
            : throw new NullReferenceException($"Slot with id: {slotID} not found");

        public void AddSlot(ISlot slot)
        {
            _slots.Add(slot.SlotID, slot);
        }

        public ISlot GetFirstEmptySlot() => _slots.FirstOrDefault(x => !x.Value.ContainItem).Value;

        public ISlot GetSlotWithItemType(EItem itemType)
        {
            foreach (var slot in _slots.Values)
            {
                if (!slot.ContainItem) continue;
                if (slot.SlotItem.ItemType != itemType) continue;
                return slot;
            }
            return null;
        }

        public ISlot GetSlotWithItem(IItem item)
        {
            foreach (var slot in _slots.Values)
            {
                if (!slot.ContainItem) continue;
                if (slot.SlotItem != item) continue;
                return slot;
            }
            return null;
        }
    }
}