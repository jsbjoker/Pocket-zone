using System;
using Common.Mediators.Initialize.Interfaces;
using Game.Inventories.Interfaces;
using Game.UI.Enums;
using Game.UI.Interfaces;

namespace Game.Inventories.Controllers
{
    public class InventoryPresenter : IInventoryPresenter, IInitializeListener, IDisposable
    {
        private readonly IInventoryView _inventoryView;
        private readonly IUIManager _uiManager;

        public InventoryPresenter(IInventoryView inventoryView, IUIManager uiManager)
        {
            _inventoryView = inventoryView;
            _uiManager = uiManager;
            _inventoryView.OnInventoryOpen += OpenInventory;
            _inventoryView.OnInventoryClose += CloseInventory;
        }

        private void OpenInventory()
        {
            _uiManager.ShowWindow(EWindow.Inventory);
        }
        
        private void CloseInventory()
        {
            _uiManager.CloseWindow(EWindow.Inventory);
        }

        public void Initialize()
        {
            
        }

        public void Dispose()
        {
            _inventoryView.OnInventoryOpen -= OpenInventory;
            _inventoryView.OnInventoryClose -= CloseInventory;
        }
    }
}