using Common.Mediators.Initialize.Interfaces;
using Game.Inventories.Interfaces;

namespace Game.Inventories.Controllers
{
    public class SlotInitializer : ISlotInitializer, IInitializeListener
    {
        private readonly ISlotFactory _slotFactory;
        private readonly IInventoryView _inventoryView;

        public SlotInitializer(ISlotFactory slotFactory, IInventoryView inventoryView)
        {
            _slotFactory = slotFactory;
            _inventoryView = inventoryView;
        }

        public void Initialize()
        {
            for (var i = 0; i < _inventoryView.SlotViews.Count; i++)
                _slotFactory.CreateSlot(i, _inventoryView.SlotViews[i]);
        }
    }
}