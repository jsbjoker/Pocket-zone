using Game.Inventories.Interfaces;
using Game.Items.Enums;
using Game.Items.Interfaces;
using Game.Items.Settings;

namespace Game.Inventories.Controllers
{
    public class Inventory : IInventory
    {
        private readonly ISlotStorage _slotStorage;
        private readonly IItemFactory _itemFactory;
        private readonly ItemSettings _itemSettings;

        public Inventory(ISlotStorage slotStorage, IItemFactory itemFactory, ItemSettings itemSettings)
        {
            _slotStorage = slotStorage;
            _itemFactory = itemFactory;
            _itemSettings = itemSettings;
        }

        public void AddItem(EItem itemType)
        {
            AddItem(itemType, 1);
        }

        public void AddItem(EItem itemType, int amount)
        {
            var isItemStackable = _itemSettings.GetItemStackable(itemType);
            if (isItemStackable)
            {
                var targetSlot = _slotStorage.GetSlotWithItemType(itemType);
                if (targetSlot != null)
                    targetSlot.AddItemAmount(amount);
                else
                    CreateNewItem(itemType, amount);
                return;
            }
            CreateNewItem(itemType, 1);
        }

        private void CreateNewItem(EItem item, int amount)
        {
            var slot = _slotStorage.GetFirstEmptySlot();
            slot?.SetSlotItem(_itemFactory.CreateItem(item, amount));
        }
        
        public void RemoveItem(IItem item)
        {
            var slot = _slotStorage.GetSlotWithItem(item);
            if (slot == null) return;
            slot.RemoveItemFromSlot();
            SortItems(slot.SlotID);
        }

        private void SortItems(int fromID)
        {
            for (var i = fromID; i < _slotStorage.Slots.Count; i++)
            {
                if (i + 1 > _slotStorage.Slots.Count) break;
                var tempSlot = _slotStorage[i];
                var nextSlot = _slotStorage[i + 1];
                if (!nextSlot.ContainItem) break;
                tempSlot.SetSlotItem(nextSlot.SlotItem);
                nextSlot.RemoveItemFromSlot();
            }
        }
    }
}