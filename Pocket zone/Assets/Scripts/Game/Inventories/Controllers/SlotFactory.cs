using Game.Inventories.Interfaces;
using Game.Inventories.Views;

namespace Game.Inventories.Controllers
{
    public class SlotFactory : ISlotFactory
    {
        private readonly ISlotStorage _slotStorage;
        private readonly IItemInfoPresenter _itemInfoPresenter;

        public SlotFactory(ISlotStorage slotStorage, IItemInfoPresenter itemInfoPresenter)
        {
            _slotStorage = slotStorage;
            _itemInfoPresenter = itemInfoPresenter;
        }


        public void CreateSlot(int slotID, SlotView slotView)
        {
            var slot = new Slot(slotID, slotView, _itemInfoPresenter);
            _slotStorage.AddSlot(slot);
        }
    }
}