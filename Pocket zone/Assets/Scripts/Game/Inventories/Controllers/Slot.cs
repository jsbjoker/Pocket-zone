using Game.Inventories.Interfaces;
using Game.Items.Interfaces;
using UnityEngine;

namespace Game.Inventories.Controllers
{
    public class Slot : ISlot
    {
        private readonly ISlotView _slotView;
        private readonly IItemInfoPresenter _itemInfoPresenter;
        public int SlotID { get; }
        public bool ContainItem => SlotItem != null;
        public IItem SlotItem { get; private set; }
        public int ItemAmount => SlotItem?.Amount ?? 0;
        
        
        public Slot(int slotID, ISlotView slotView, IItemInfoPresenter itemInfoPresenter)
        {
            _slotView = slotView;
            _itemInfoPresenter = itemInfoPresenter;
            SlotID = slotID;
            RemoveItemFromSlot();
            _slotView.OnSlotClick += OnSlotClick;
        }

        private void OnSlotClick()
        {
            if (!ContainItem) return;
            _itemInfoPresenter.ShowItemInfo(SlotItem);
        }

        public void SetSlotItem(IItem item)
        {
            SlotItem = item;
            _slotView.SetItemImage(item.ItemSprite);
            _slotView.SetItemImageActiveState(true);
            _slotView.SetItemAmount(ItemAmount <= 1 ? string.Empty : ItemAmount.ToString());
        }

        public void AddItemAmount(int itemAmount)
        {
            SlotItem.SetItemAmount(ItemAmount + itemAmount);
            _slotView.SetItemAmount(ItemAmount <= 1 ? string.Empty : ItemAmount.ToString());
        }

        public void RemoveItemFromSlot()
        {
            SlotItem = null;
            _slotView.SetItemAmount(string.Empty);
            _slotView.SetItemImageActiveState(false);
        }

        public void Dispose()
        {
            _slotView.OnSlotClick -= OnSlotClick;
        }
    }
}