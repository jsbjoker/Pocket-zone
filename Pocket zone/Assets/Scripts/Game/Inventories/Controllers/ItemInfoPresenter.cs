using System;
using Game.Inventories.Interfaces;
using Game.Items.Interfaces;
using Game.UI.Enums;
using Game.UI.Interfaces;

namespace Game.Inventories.Controllers
{
    public class ItemInfoPresenter : IItemInfoPresenter, IDisposable
    {
        private readonly IItemInfoView _itemInfoView;
        private readonly IUIManager _uiManager;
        private readonly IInventory _inventory;
        private IItem _observeItem;

        public ItemInfoPresenter(IItemInfoView itemInfoView, IUIManager uiManager, IInventory inventory)
        {
            _itemInfoView = itemInfoView;
            _uiManager = uiManager;
            _inventory = inventory;
            _itemInfoView.OnCloseClick += CloseItemInfo;
            _itemInfoView.OnRemoveClick += RemoveItem;
        }

        private void RemoveItem()
        {
            _inventory.RemoveItem(_observeItem);
            CloseItemInfo();
        }

        private void CloseItemInfo()
        {
            _uiManager.CloseWindow(EWindow.ItemInfo);
        }

        public void ShowItemInfo(IItem item)
        {
            _observeItem = item;
            _itemInfoView.SetItemAmount(item.Amount <= 1 ? string.Empty : item.Amount.ToString());
            _itemInfoView.SetItemIcon(item.ItemSprite);
            _uiManager.ShowWindow(EWindow.ItemInfo);
        }

        public void Dispose()
        {
            _itemInfoView.OnCloseClick -= CloseItemInfo;
            _itemInfoView.OnRemoveClick -= RemoveItem;
        }
    }
}