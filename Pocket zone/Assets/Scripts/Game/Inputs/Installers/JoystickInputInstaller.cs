using Game.Inputs.Controllers;
using UnityEngine;
using Zenject;

namespace Game.Inputs.Installers
{
    public class JoystickInputInstaller : MonoInstaller
    {
        [SerializeField] private Joystick _joystick;
        
        public override void InstallBindings()
        {
            Container.BindInstance(_joystick).AsSingle();
            Container.BindInterfacesTo<JoystickInputListener>().AsSingle();
        }
    }
}