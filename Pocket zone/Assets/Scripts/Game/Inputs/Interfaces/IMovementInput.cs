using System;
using UnityEngine;

namespace Game.Inputs.Interfaces
{
    public interface IMovementInput
    {
        event Action<Vector2> OnMove;
    }
}