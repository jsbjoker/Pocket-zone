using System;
using Common.Mediators.Update.Interfaces;
using Game.Inputs.Interfaces;
using UnityEngine;

namespace Game.Inputs.Controllers
{
    public class JoystickInputListener : IMovementInput, IUpdateListener, IDisposable
    {
        private readonly Joystick _joystick;
        public event Action<Vector2> OnMove;

        public JoystickInputListener(Joystick joystick)
        {
            _joystick = joystick;
        }
        
        public void Update(float deltaTime)
        {
            if (_joystick.Direction == Vector2.zero) return;
            OnMove?.Invoke(_joystick.Direction);
        }


        public void Dispose()
        {
            OnMove = null;
        }
    }
}