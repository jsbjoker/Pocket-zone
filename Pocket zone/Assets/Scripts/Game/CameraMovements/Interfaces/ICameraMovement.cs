using UnityEngine;

namespace Game.CameraMovements.Interfaces
{
    public interface ICameraMovement
    {
        void SetFollowTarget(Transform followTransform);
    }
}