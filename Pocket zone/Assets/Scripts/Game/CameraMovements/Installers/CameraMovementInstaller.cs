using Cinemachine;
using Game.CameraMovements.Controllers;
using UnityEngine;
using Zenject;

namespace Game.CameraMovements.Installers
{
    public class CameraMovementInstaller : MonoInstaller
    {
        [SerializeField] private CinemachineVirtualCamera _virtualCamera;

        public override void InstallBindings()
        {
            Container.BindInstance(_virtualCamera).AsSingle();
            Container.BindInterfacesTo<CinemachineCameraMovement>().AsSingle();
        }
    }
}