using Cinemachine;
using Game.CameraMovements.Interfaces;
using UnityEngine;

namespace Game.CameraMovements.Controllers
{
    public class CinemachineCameraMovement : ICameraMovement
    {
        private readonly CinemachineVirtualCamera _cinemachineVirtualCamera;

        public CinemachineCameraMovement(CinemachineVirtualCamera cinemachineVirtualCamera)
        {
            _cinemachineVirtualCamera = cinemachineVirtualCamera;
        }

        public void SetFollowTarget(Transform followTransform)
        {
            _cinemachineVirtualCamera.Follow = followTransform;
        }
    }
}