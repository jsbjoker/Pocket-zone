using System;
using Game.WorldStates.Enums;

namespace Game.WorldStates.Interfaces
{
    public interface IWorldState
    {
        EWorldState CurrentState { get; }
        void SetWorldState(EWorldState state);
        event Action<EWorldState> OnWorldStateChange;
    }
}