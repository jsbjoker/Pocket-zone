namespace Game.WorldStates.Enums
{
    public enum EWorldState
    {
        Initialize = 0,
        StartGame = 1
    }
}