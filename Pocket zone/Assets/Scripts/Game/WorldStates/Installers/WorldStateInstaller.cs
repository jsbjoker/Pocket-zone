using Game.WorldStates.Controllers;
using UnityEngine;
using Zenject;

namespace Game.WorldStates.Installers
{
    [CreateAssetMenu(fileName = "WorldStateInstaller", menuName = "Installers/Game/WorldStateInstaller")]
    public class WorldStateInstaller : ScriptableObjectInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesTo<WorldState>().AsSingle();
            Container.BindInterfacesTo<WorldStateInitializer>().AsSingle();
        }
    }
}