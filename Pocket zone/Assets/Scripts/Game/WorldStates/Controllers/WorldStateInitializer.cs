using Common.Mediators.Initialize.Interfaces;
using Game.WorldStates.Enums;
using Game.WorldStates.Interfaces;

namespace Game.WorldStates.Controllers
{
    public class WorldStateInitializer : IWorldStateInitializer, IInitializeListener
    {
        private readonly IWorldState _worldState;

        public WorldStateInitializer(IWorldState worldState)
        {
            _worldState = worldState;
        }

        public void Initialize()
        {
            _worldState.SetWorldState(EWorldState.Initialize);
            _worldState.SetWorldState(EWorldState.StartGame);
        }
    }
}