using System;
using System.Collections.Generic;
using Common.Mediators.Update.Interfaces;
using Game.WorldStates.Enums;
using Game.WorldStates.Interfaces;

namespace Game.WorldStates.Controllers
{
    public class WorldState : IWorldState, IUpdateListener, IDisposable
    {
        private readonly Queue<EWorldState> _notifyQueue = new ();
        public EWorldState CurrentState { get; private set; }
        private bool _isNotifying;
        
        public event Action<EWorldState> OnWorldStateChange;

        public void SetWorldState(EWorldState state)
        {
            _notifyQueue.Enqueue(state);
        }
        
        public void Update(float deltaTime)
        {
            if (_notifyQueue.Count < 1) return;
            while (_notifyQueue.Count > 0)
            {
                CurrentState = _notifyQueue.Dequeue();
                OnWorldStateChange?.Invoke(CurrentState);
            }
        }

        public void Dispose()
        {
            OnWorldStateChange = null;
        }

        
    }
}