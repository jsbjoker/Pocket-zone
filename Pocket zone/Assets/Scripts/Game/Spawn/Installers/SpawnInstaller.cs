using Game.Spawn.Controllers;
using UnityEngine;
using Zenject;

namespace Game.Spawn.Installers
{
    [CreateAssetMenu(fileName = "SpawnInstaller", menuName = "Installers/Game/SpawnInstaller")]
    public class SpawnInstaller : ScriptableObjectInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesTo<EnemySpawn>().AsSingle();
            Container.BindInterfacesTo<PlayerSpawn>().AsSingle();
        }
    }
}