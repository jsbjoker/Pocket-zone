using System;
using Common.Mediators.Initialize.Interfaces;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.Spawn.Interfaces;
using Game.WorldStates.Enums;
using Game.WorldStates.Interfaces;

namespace Game.Spawn.Controllers
{
    public class PlayerSpawn : IPlayerSpawn, IInitializeListener, IDisposable
    {
        private readonly IWorldState _worldState;
        private readonly ICharacterFactory _characterFactory;

        public PlayerSpawn(IWorldState worldState, ICharacterFactory characterFactory)
        {
            _worldState = worldState;
            _characterFactory = characterFactory;
            _worldState.OnWorldStateChange += OnWorldStateChange;
        }

        private void OnWorldStateChange(EWorldState worldState)
        {
            if (worldState != EWorldState.Initialize) return;
            _characterFactory.CreateCharacter(ECharacterType.Player);
        }

        public void Initialize()
        {
            
        }
        
        public void Dispose()
        {
            _worldState.OnWorldStateChange -= OnWorldStateChange;
        }
    }
}