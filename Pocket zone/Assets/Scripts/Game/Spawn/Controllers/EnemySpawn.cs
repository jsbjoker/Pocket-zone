using System;
using Common.Mediators.Update.Interfaces;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.Spawn.Interfaces;
using Game.WorldStates.Enums;
using Game.WorldStates.Interfaces;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game.Spawn.Controllers
{
    public class EnemySpawn : IEnemySpawn, IUpdateListener, IDisposable
    {
        private readonly ICharacterFactory _characterFactory;
        private readonly ICharacterStorage _characterStorage;
        private readonly IWorldState _worldState;

        public EnemySpawn(ICharacterFactory characterFactory, ICharacterStorage characterStorage, IWorldState worldState)
        {
            _characterFactory = characterFactory;
            _characterStorage = characterStorage;
            _worldState = worldState;
            _worldState.OnWorldStateChange += OnWorldStateChange;
        }

        private void OnWorldStateChange(EWorldState worldState)
        {
            return;
            if (worldState != EWorldState.StartGame) return;
            for (var i = 0; i < 3; i++)
                SpawnEnemy();
        }

        public void SpawnEnemy()
        {
            var xSide = GetRandomNegativeOrPositiveValue();
            var xPosition = Random.Range(_characterStorage.Player.Position.x + 5f * xSide,
                _characterStorage.Player.Position.x + 5f + 10f * xSide);
            var ySide = GetRandomNegativeOrPositiveValue();
            var yPosition = Random.Range(_characterStorage.Player.Position.y + 5f * ySide,
                _characterStorage.Player.Position.y + 5f + 10f * ySide);
            _characterFactory.CreateCharacter(ECharacterType.Flesh, new Vector2(xPosition, yPosition));
        }

        private int GetRandomNegativeOrPositiveValue() => Random.Range(0, 2) == 0 ? -1 : 1;

        public void Update(float deltaTime)
        {
            if (_characterStorage.Player == null) return;
            if (!_characterStorage.Player.Model.IsAlive) return;
            if (_characterStorage.Enemies.Count > 2) return;
            SpawnEnemy();
        }

        public void Dispose()
        {
            _worldState.OnWorldStateChange -= OnWorldStateChange;
        }
    }
}