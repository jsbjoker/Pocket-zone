namespace Game.Spawn.Interfaces
{
    public interface IEnemySpawn
    {
        void SpawnEnemy();
    }
}