namespace Game.Characters.Enums
{
    public enum ECharacterParam
    {
        Hp,
        MaxHp,
        MoveSpeed
    }
}