using System;
using System.Collections.Generic;
using Game.Characters.Interfaces;

namespace Game.Characters.Controllers
{
    public class CharacterStorage : ICharacterStorage, IDisposable
    {
        private readonly List<ICharacter> _enemies = new();
        public IReadOnlyList<ICharacter> Enemies => _enemies;
        public IPlayer Player { get; private set; }
        public event Action<ICharacter> OnEnemyAdd;
        public event Action<ICharacter> OnEnemyRemove;
        public event Action<IPlayer> OnPlayerAdd;
        public event Action<IPlayer> OnPlayerRemove;

        public void AddEnemy(ICharacter character)
        {
            _enemies.Add(character);
            OnEnemyAdd?.Invoke(character);
        }

        public void RemoveEnemy(ICharacter character)
        {
            _enemies.Remove(character);
            OnEnemyRemove?.Invoke(character);
        }

        public void SetPlayer(IPlayer player)
        {
            Player = player;
            OnPlayerAdd?.Invoke(player);
        }

        public void RemovePlayer()
        {
            var player = Player;
            Player = null;
            OnPlayerRemove?.Invoke(player);
        }

        public void Dispose()
        {
            OnEnemyAdd = null;
            OnEnemyRemove = null;
            OnPlayerAdd = null;
            OnPlayerRemove = null;
        }
    }
}