using System;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using UnityEngine;

namespace Game.Characters.Controllers
{
    public class Character : ICharacter
    {
        private readonly ICharacterView _characterView;
        private readonly ICharacterMovement _characterMovement;
        public IParamModel Model { get; }
        public ETeam Team { get; }
        public ECharacterType Type { get; }
        public event Action<ICharacter> OnCharacterDie;

        public Character(IParamModel model, ICharacterView characterView, ETeam team,
            ECharacterType type, ICharacterMovement characterMovement)
        {
            _characterView = characterView;
            _characterMovement = characterMovement;
            Team = team;
            Type = type;
            Model = model;
        }

        public void MoveTo(Vector3 direction) => _characterMovement.MoveTo(direction);

        public Vector3 Position => _characterMovement.Position;

        public virtual void Dispose()
        {
            OnCharacterDie?.Invoke(this);
            Model?.Dispose();
        }
    }
}