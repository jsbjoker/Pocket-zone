using System.Collections.Generic;
using Game.CameraMovements.Interfaces;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.Characters.Settings;
using Game.Characters.Views;
using UnityEngine;

namespace Game.Characters.Controllers
{
    public class CharacterFactory : ICharacterFactory
    {
        private readonly CharacterViewSettings _viewSettings;
        private readonly ICharacterStorage _characterStorage;
        private readonly ICameraMovement _cameraMovement;

        public CharacterFactory(CharacterViewSettings viewSettings, ICharacterStorage characterStorage, ICameraMovement cameraMovement)
        {
            _viewSettings = viewSettings;
            _characterStorage = characterStorage;
            _cameraMovement = cameraMovement;
        }

        public ICharacter CreateCharacter(ECharacterType characterType)
        {
            return CreateCharacter(characterType, Vector2.zero);
        }

        public ICharacter CreateCharacter(ECharacterType characterType, Vector2 position)
        {
            var view = Object.Instantiate(_viewSettings.GetCharacterView(characterType), position, Quaternion.identity);
            var model = new ParamModel(new Dictionary<ECharacterParam, float>()
            {
                [ECharacterParam.Hp] = 100f,
                [ECharacterParam.MaxHp] = 100f,
                [ECharacterParam.MoveSpeed] = 6f
            });
            var team = characterType == ECharacterType.Player ? ETeam.Player : ETeam.Enemy;
            var movement = new CharacterMovement(model, view);
            return team == ETeam.Player 
                ? CreatePlayer(model, view, team, characterType, movement) 
                : CreateEnemy(model, view, team, characterType, movement);
        }

        private ICharacter CreatePlayer(IParamModel model, CharacterView view, ETeam team, ECharacterType type, ICharacterMovement movement)
        {
            var character = new Player(model, view, team, type, movement);
            _cameraMovement.SetFollowTarget(view.transform);
            _characterStorage.SetPlayer(character);
            return character;
        }
        
        private ICharacter CreateEnemy(IParamModel model, ICharacterView view, ETeam team, ECharacterType type, ICharacterMovement movement)
        {
            var character = new Character(model, view, team, type, movement);
            _characterStorage.AddEnemy(character);
            return character;
        }
    }
}