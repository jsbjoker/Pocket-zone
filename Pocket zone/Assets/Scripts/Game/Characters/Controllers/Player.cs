using Game.Characters.Enums;
using Game.Characters.Interfaces;

namespace Game.Characters.Controllers
{
    public class Player : Character, IPlayer
    {
        public Player(IParamModel model, ICharacterView characterView, ETeam team, ECharacterType type, ICharacterMovement movement)
            : base(model, characterView, team, type, movement)
        {
            
        }
    }
}