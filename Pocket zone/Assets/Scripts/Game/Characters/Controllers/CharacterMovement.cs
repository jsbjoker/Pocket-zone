using Game.Characters.Enums;
using Game.Characters.Interfaces;
using UnityEngine;

namespace Game.Characters.Controllers
{
    public class CharacterMovement : ICharacterMovement
    {
        private readonly IParamModel _paramModel;
        private readonly ICharacterView _characterView;

        public CharacterMovement(IParamModel paramModel, ICharacterView characterView)
        {
            _paramModel = paramModel;
            _characterView = characterView;
        }

        public void MoveTo(Vector3 direction) 
            => _characterView.SetPosition(Position + direction * Time.deltaTime * _paramModel[ECharacterParam.MoveSpeed]);

        public Vector3 Position => _characterView.Position;
    }
}