using System;
using System.Collections.Generic;
using Game.Characters.Enums;
using Game.Characters.Interfaces;

namespace Game.Characters.Controllers
{
    public class ParamModel : IParamModel
    {
        private readonly Dictionary<ECharacterParam, float> _params;
        
        public event Action<ECharacterParam, float> OnParamChange;

        public ParamModel(Dictionary<ECharacterParam, float> characterParams)
        {
            _params = characterParams;
        }

        public float this[ECharacterParam param] => _params.ContainsKey(param) ? _params[param] : 0f;

        public bool IsAlive => this[ECharacterParam.Hp] > 0f;
        
        
        public void SetParamValue(ECharacterParam param, float value)
        {
            _params[param] = value;
            OnParamChange?.Invoke(param, value);
        }

        public void AddParamValue(ECharacterParam param, float value)
        {
            _params[param] = this[param] + value;
            OnParamChange?.Invoke(param, _params[param]);
        }

        public void SpendParamValue(ECharacterParam param, float value)
        {
            _params[param] = this[param] - value;
            OnParamChange?.Invoke(param, _params[param]);
        }

        public void Dispose()
        {
            OnParamChange = null;
        }
    }
}