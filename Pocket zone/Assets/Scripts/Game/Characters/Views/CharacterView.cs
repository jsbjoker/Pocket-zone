using Game.Characters.Interfaces;
using UnityEngine;

namespace Game.Characters.Views
{
    public class CharacterView : MonoBehaviour, ICharacterView
    {
        
        
        public void SetPosition(Vector2 targetPosition)
        {
            transform.position = targetPosition;
        }

        public Vector2 Position => transform.position;
    }
}