using UnityEngine;

namespace Game.Characters.Interfaces
{
    public interface ICharacterMovement
    {
        void MoveTo(Vector3 direction);
        Vector3 Position { get; }
    }
}