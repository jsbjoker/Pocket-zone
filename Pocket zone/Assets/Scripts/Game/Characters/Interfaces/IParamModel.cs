using System;
using Game.Characters.Enums;

namespace Game.Characters.Interfaces
{
    public interface IParamModel : IDisposable
    {
        float this[ECharacterParam param] { get; }
        bool IsAlive { get; }
        void SetParamValue(ECharacterParam param, float value);
        void AddParamValue(ECharacterParam param, float value);
        void SpendParamValue(ECharacterParam param, float value);
        event Action<ECharacterParam, float> OnParamChange;
    }
}