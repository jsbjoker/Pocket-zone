using System;
using System.Collections.Generic;

namespace Game.Characters.Interfaces
{
    public interface ICharacterStorage
    {
        IReadOnlyList<ICharacter> Enemies { get; }
        IPlayer Player { get; }
        event Action<ICharacter> OnEnemyAdd;
        event Action<ICharacter> OnEnemyRemove;
        event Action<IPlayer> OnPlayerAdd;
        event Action<IPlayer> OnPlayerRemove;
        void AddEnemy(ICharacter character);
        void RemoveEnemy(ICharacter character);
        void SetPlayer(IPlayer player);
        void RemovePlayer();
        
    }
}