using UnityEngine;

namespace Game.Characters.Interfaces
{
    public interface ICharacterView
    {
        void SetPosition(Vector2 targetPosition);
        Vector2 Position { get; }
    }
}