using System;
using Game.Characters.Enums;

namespace Game.Characters.Interfaces
{
    public interface ICharacter : ICharacterMovement,  IDisposable
    {
        IParamModel Model { get; }
        ETeam Team { get; }
        ECharacterType Type { get; }
        event Action<ICharacter> OnCharacterDie;
    }
}