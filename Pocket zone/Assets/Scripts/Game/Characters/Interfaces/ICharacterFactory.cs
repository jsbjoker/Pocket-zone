using Game.Characters.Enums;
using UnityEngine;

namespace Game.Characters.Interfaces
{
    public interface ICharacterFactory
    {
        ICharacter CreateCharacter(ECharacterType character);
        ICharacter CreateCharacter(ECharacterType character, Vector2 position);
    }
}