using System;
using Game.Characters.Enums;
using Game.Characters.Views;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

namespace Game.Characters.Settings
{
    [CreateAssetMenu(fileName = "CharacterViewSettings", menuName = "Settings/Game/CharacterViewSettings")]
    public class CharacterViewSettings : ScriptableObject
    {
        [SerializeField] private CharacterViewDictionary _characterViews;

        public CharacterView GetCharacterView(ECharacterType character) => _characterViews.ContainsKey(character)
            ? _characterViews[character]
            : throw new NullReferenceException($"{character} view not found");
    }

    [Serializable]
    public class CharacterViewDictionary : SerializableDictionaryBase<ECharacterType, CharacterView> {}
}