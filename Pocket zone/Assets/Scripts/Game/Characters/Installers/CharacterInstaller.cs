using Game.Characters.Controllers;
using Game.Characters.Settings;
using UnityEngine;
using Zenject;

namespace Game.Characters.Installers
{
    [CreateAssetMenu(fileName = "CharacterInstaller", menuName = "Installers/Game/CharacterInstaller")]
    public class CharacterInstaller : ScriptableObjectInstaller
    {
        [SerializeField] private CharacterViewSettings _characterView;

        public override void InstallBindings()
        {
            Container.BindInstance(_characterView).AsSingle();
            Container.BindInterfacesTo<CharacterFactory>().AsSingle();
            Container.BindInterfacesTo<CharacterStorage>().AsSingle();
        }
    }
}