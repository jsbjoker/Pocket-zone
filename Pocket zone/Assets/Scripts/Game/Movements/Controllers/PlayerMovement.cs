using System;
using Common.Mediators.Initialize.Interfaces;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.Inputs.Interfaces;
using Game.Movements.Interfaces;
using UnityEngine;

namespace Game.Movements.Controllers
{
    public class PlayerMovement : IPlayerMovement, IInitializeListener, IDisposable
    {
        private readonly ICharacterStorage _characterStorage;
        private readonly IMovementInput _movementInput;

        public PlayerMovement(ICharacterStorage characterStorage, IMovementInput movementInput)
        {
            _characterStorage = characterStorage;
            _movementInput = movementInput;
            _movementInput.OnMove += MovePlayer;
        }

        private void MovePlayer(Vector2 direction)
        {
            var player = _characterStorage.Player;
            if (player == null) return;
            if (!player.Model.IsAlive) return;
            _characterStorage.Player.MoveTo(direction);
        }

        public void Initialize()
        {
            
        }

        public void Dispose()
        {
            _movementInput.OnMove -= MovePlayer;
        }
    }
}