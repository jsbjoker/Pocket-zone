using Game.Movements.Controllers;
using UnityEngine;
using Zenject;

namespace Game.Movements.Installers
{
    [CreateAssetMenu(fileName = "MovementInstaller", menuName = "Installers/Game/MovementInstaller")]
    public class MovementInstaller : ScriptableObjectInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesTo<PlayerMovement>().AsSingle();
        }
    }
}