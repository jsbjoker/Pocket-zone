using Common.Mediators.Update.Interfaces;
using Game.Characters.Interfaces;
using Game.EnemyMovements.Interfaces;
using UnityEngine;

namespace Game.EnemyMovements.Controllers
{
    public class EnemyMovement : IEnemyMovement, IUpdateListener
    {
        private readonly ICharacterStorage _characterStorage;

        public EnemyMovement(ICharacterStorage characterStorage)
        {
            _characterStorage = characterStorage;
        }

        public void Update(float deltaTime)
        {
            if (_characterStorage.Player == null) return;
            if (!_characterStorage.Player.Model.IsAlive) return;
            foreach (var enemy in _characterStorage.Enemies)
            {
                var distance = Vector2.Distance(_characterStorage.Player.Position, enemy.Position);
                if (distance > 5f || distance <= 2f) continue;
                var enemyDirection = (_characterStorage.Player.Position - enemy.Position).normalized;
                enemy.MoveTo(enemyDirection);
            }
        }
    }
}