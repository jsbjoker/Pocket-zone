using Game.EnemyMovements.Controllers;
using UnityEngine;
using Zenject;

namespace Game.EnemyMovements.Installers
{
    [CreateAssetMenu(fileName = "EnemyMovementInstaller", menuName = "Installers/Game/EnemyMovementInstaller")]
    public class EnemyMovementInstaller : ScriptableObjectInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesTo<EnemyMovement>().AsSingle();
        }
    }
}