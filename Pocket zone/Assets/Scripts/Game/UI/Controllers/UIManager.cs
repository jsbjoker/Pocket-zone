using Game.UI.Enums;
using Game.UI.Interfaces;

namespace Game.UI.Controllers
{
    public class UIManager : IUIManager
    {
        private readonly IWindowStorage _windowStorage;

        public UIManager(IWindowStorage windowStorage)
        {
            _windowStorage = windowStorage;
        }

        public void ShowWindow(EWindow window)
        {
            _windowStorage[window]?.SetWindowActiveState(true);
        }

        public void CloseWindow(EWindow window)
        {
            _windowStorage[window]?.SetWindowActiveState(false);
        }
    }
}