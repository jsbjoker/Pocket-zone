using System.Collections.Generic;
using Common.Mediators.Initialize.Interfaces;
using Game.UI.Enums;
using Game.UI.Interfaces;

namespace Game.UI.Controllers
{
    public class UIInitializer : IUIInitializer, IInitializeListener
    {
        private readonly List<IWindowView> _windows;
        private readonly IWindowStorage _windowStorage;

        public UIInitializer(List<IWindowView> windows, IWindowStorage windowStorage)
        {
            _windows = windows;
            _windowStorage = windowStorage;
        }

        public void Initialize()
        {
            foreach (var window in _windows)
                _windowStorage.AddWindow(window);

            foreach (var window in _windowStorage.Windows.Values)
            {
                window.SetWindowActiveState(window.Window == EWindow.None);
            }
        }
    }
}