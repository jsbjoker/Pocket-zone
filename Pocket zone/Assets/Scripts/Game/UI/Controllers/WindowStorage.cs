using System;
using System.Collections.Generic;
using Game.UI.Enums;
using Game.UI.Interfaces;

namespace Game.UI.Controllers
{
    public class WindowStorage : IWindowStorage
    {
        private readonly Dictionary<EWindow, IWindowView> _windows = new();

        public IWindowView this[EWindow window] => _windows.ContainsKey(window)
            ? _windows[window]
            : throw new NullReferenceException($"Window: {window} not found");

        public IReadOnlyDictionary<EWindow, IWindowView> Windows => _windows;


        public void AddWindow(IWindowView windowView)
        {
            _windows.Add(windowView.Window, windowView);
        }
    }
}