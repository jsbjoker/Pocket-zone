using Game.UI.Enums;

namespace Game.UI.Interfaces
{
    public interface IWindowView
    {
        EWindow Window { get; }
        void SetWindowActiveState(bool value);
    }
}