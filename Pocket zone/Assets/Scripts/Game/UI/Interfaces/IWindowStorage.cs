using System.Collections.Generic;
using Game.UI.Enums;

namespace Game.UI.Interfaces
{
    public interface IWindowStorage
    {
        IWindowView this[EWindow window] { get; }
        IReadOnlyDictionary<EWindow, IWindowView> Windows { get; }
        void AddWindow(IWindowView windowView);
    }
}