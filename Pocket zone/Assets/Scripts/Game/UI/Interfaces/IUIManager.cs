using Game.UI.Enums;

namespace Game.UI.Interfaces
{
    public interface IUIManager
    {
        public void ShowWindow(EWindow window);
        public void CloseWindow(EWindow window);
    }
}