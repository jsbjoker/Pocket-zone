namespace Game.UI.Enums
{
    public enum EWindow
    {
        Inventory = 0,
        ItemInfo = 1,
        None = 2
    }
}