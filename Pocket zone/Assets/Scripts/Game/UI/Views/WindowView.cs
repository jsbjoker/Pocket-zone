using Game.UI.Enums;
using Game.UI.Interfaces;
using UnityEngine;

namespace Game.UI.Views
{
    public class WindowView : MonoBehaviour, IWindowView
    {
        [SerializeField] private EWindow _window;

        public EWindow Window => _window;
        public void SetWindowActiveState(bool value) => gameObject.SetActive(value);
    }
}