using Game.UI.Controllers;
using Zenject;

namespace Game.UI.Installers
{
    public class UIInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesTo<UIManager>().AsSingle();
            Container.BindInterfacesTo<UIInitializer>().AsSingle();
            Container.BindInterfacesTo<WindowStorage>().AsSingle();
        }
    }
}