using UnityEngine;

namespace Game.HealthBars.Interfaces
{
    public interface IHealthBarParentView
    {
        RectTransform HealthBarParentRect { get; }
    }
}