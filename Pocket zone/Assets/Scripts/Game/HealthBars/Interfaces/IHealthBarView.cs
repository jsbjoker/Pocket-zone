using UnityEngine;

namespace Game.HealthBars.Interfaces
{
    public interface IHealthBarView
    {
        void SetHealthBarPosition(Vector2 position);
        void SetHealthBarFillAmount(float value);
    }
}