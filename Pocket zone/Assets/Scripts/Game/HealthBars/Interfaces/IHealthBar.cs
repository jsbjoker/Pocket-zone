using System;
using UnityEngine;

namespace Game.HealthBars.Interfaces
{
    public interface IHealthBar : IDisposable
    {
        Vector3 CharacterPosition { get; }
        void SetHealthBarPosition(Vector2 position);
    }
}