using Game.Characters.Interfaces;

namespace Game.HealthBars.Interfaces
{
    public interface IHealthBarFactory
    {
        void CreateHealthBar(ICharacter character);
    }
}