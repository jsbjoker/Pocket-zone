using System.Collections.Generic;
using Game.Characters.Interfaces;

namespace Game.HealthBars.Interfaces
{
    public interface IHealthBarStorage
    {
        IHealthBar this[ICharacter character] { get; }
        IReadOnlyDictionary<ICharacter, IHealthBar> HealthBars { get; }
        void AddHealthBar(ICharacter character, IHealthBar healthBar);
        void RemoveHealthBar(ICharacter healthBar);
    }
}