using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.HealthBars.Interfaces;
using UnityEngine;

namespace Game.HealthBars.Controllers
{
    public class HealthBar : IHealthBar
    {
        private readonly ICharacter _character;
        private readonly IHealthBarView _healthBarView;

        public Vector3 CharacterPosition => _character.Position;

        public HealthBar(ICharacter character, IHealthBarView healthBarView)
        {
            _character = character;
            _healthBarView = healthBarView;
            _character.Model.OnParamChange += OnParamChange;
            SetHealtBarFill();
        }

        private void OnParamChange(ECharacterParam param, float value)
        {
            if (param != ECharacterParam.MaxHp && param != ECharacterParam.Hp) return;
            SetHealtBarFill();
        }

        private void SetHealtBarFill()
        {
            _healthBarView.SetHealthBarFillAmount(_character.Model[ECharacterParam.Hp] / _character.Model[ECharacterParam.MaxHp]);
        }
        
        public void SetHealthBarPosition(Vector2 position) => _healthBarView.SetHealthBarPosition(position);

        public void Dispose()
        {
            _character.Model.OnParamChange -= OnParamChange;
        }
    }
}