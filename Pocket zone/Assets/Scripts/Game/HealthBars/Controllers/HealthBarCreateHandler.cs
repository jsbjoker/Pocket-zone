using System;
using Common.Mediators.Initialize.Interfaces;
using Game.Characters.Interfaces;
using Game.HealthBars.Interfaces;

namespace Game.HealthBars.Controllers
{
    public class HealthBarCreateHandler : IHealthBarCreateHandler, IInitializeListener, IDisposable
    {
        private readonly ICharacterStorage _characterStorage;
        private readonly IHealthBarFactory _healthBarFactory;

        public HealthBarCreateHandler(ICharacterStorage characterStorage, IHealthBarFactory healthBarFactory)
        {
            _characterStorage = characterStorage;
            _healthBarFactory = healthBarFactory;
            _characterStorage.OnEnemyAdd += OnCharacterAdd;
            _characterStorage.OnPlayerAdd += OnCharacterAdd;
        }

        private void OnCharacterAdd(ICharacter character)
        {
            _healthBarFactory.CreateHealthBar(character);
        }

        public void Initialize()
        {
            
        }

        public void Dispose()
        {
            _characterStorage.OnEnemyAdd -= OnCharacterAdd;
            _characterStorage.OnPlayerAdd -= OnCharacterAdd;
        }
    }
}