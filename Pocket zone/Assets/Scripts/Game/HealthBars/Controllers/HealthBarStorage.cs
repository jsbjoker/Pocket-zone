using System;
using System.Collections.Generic;
using Game.Characters.Interfaces;
using Game.HealthBars.Interfaces;

namespace Game.HealthBars.Controllers
{
    public class HealthBarStorage : IHealthBarStorage
    {
        private readonly Dictionary<ICharacter, IHealthBar> _healthBars = new();

        public IHealthBar this[ICharacter character] => _healthBars.ContainsKey(character)
            ? _healthBars[character]
            : throw new NullReferenceException($"Health bar for: {character.Type} not found");

        public IReadOnlyDictionary<ICharacter, IHealthBar> HealthBars => _healthBars;
        
        public void AddHealthBar(ICharacter character, IHealthBar healthBar)
        {
            _healthBars.Add(character, healthBar);
        }

        public void RemoveHealthBar(ICharacter character)
        {
            _healthBars.Remove(character);
        }
    }
}