using Game.Characters.Interfaces;
using Game.HealthBars.Interfaces;
using Game.HealthBars.Views;
using UnityEngine;

namespace Game.HealthBars.Controllers
{
    public class HealthBarFactory : IHealthBarFactory
    {
        private readonly HealthBarView _healthBarView;
        private readonly IHealthBarStorage _healthBarStorage;
        private readonly RectTransform _parentRect;
        
        public HealthBarFactory(IHealthBarParentView healthBarParentView, HealthBarView healthBarView,
            IHealthBarStorage healthBarStorage)
        {
            _healthBarView = healthBarView;
            _healthBarStorage = healthBarStorage;
            _parentRect = healthBarParentView.HealthBarParentRect;
        }

        public void CreateHealthBar(ICharacter character)
        {
            var healthBarView = Object.Instantiate(_healthBarView, _parentRect);
            var healthBar = new HealthBar(character, healthBarView);
            _healthBarStorage.AddHealthBar(character, healthBar);
            character.OnCharacterDie += RemoveHealthBar;
        }

        private void RemoveHealthBar(ICharacter character)
        {
            character.OnCharacterDie -= RemoveHealthBar;
            _healthBarStorage.RemoveHealthBar(character);
        }
    }
}