using Common.Mediators.Update.Interfaces;
using Game.HealthBars.Interfaces;
using UnityEngine;

namespace Game.HealthBars.Controllers
{
    public class HealthBarMovement : IHealthBarMovement, IUpdateListener
    {
        private readonly IHealthBarStorage _healthBarStorage;
        private readonly RectTransform _parentRect;

        public HealthBarMovement(IHealthBarStorage healthBarStorage, IHealthBarParentView healthBarParentView)
        {
            _healthBarStorage = healthBarStorage;
            _parentRect = healthBarParentView.HealthBarParentRect;
        }

        public void Update(float deltaTime)
        {
            foreach (var healthBar in _healthBarStorage.HealthBars.Values)
            {
                healthBar.SetHealthBarPosition(GetHealthBarPosition(healthBar.CharacterPosition));
            }
        }
        
        private Vector2 GetHealthBarPosition(Vector3 position)
        {
            var screenPoint = RectTransformUtility.WorldToScreenPoint(Camera.main, position);
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_parentRect, screenPoint, null, out var localPosition);
            localPosition.y -= 75f;
            return localPosition;
        }
    }
}