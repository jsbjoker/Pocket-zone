using Game.HealthBars.Interfaces;
using UnityEngine;

namespace Game.HealthBars.Views
{
    public class HealthBarParentView : MonoBehaviour, IHealthBarParentView
    {
        [SerializeField] private RectTransform _rect;
        public RectTransform HealthBarParentRect => _rect;
    }
}