using Game.HealthBars.Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace Game.HealthBars.Views
{
    public class HealthBarView : MonoBehaviour, IHealthBarView
    {
        [SerializeField] private Image _bar;
        
        public void SetHealthBarPosition(Vector2 position)
        {
            transform.localPosition = position;
        }

        public void SetHealthBarFillAmount(float value)
        {
            _bar.fillAmount = value;
        }
    }
}