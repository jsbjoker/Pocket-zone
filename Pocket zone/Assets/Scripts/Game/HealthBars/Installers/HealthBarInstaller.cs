using Game.HealthBars.Controllers;
using Game.HealthBars.Views;
using UnityEngine;
using Zenject;

namespace Game.HealthBars.Installers
{
    public class HealthBarInstaller : MonoInstaller
    {
        [SerializeField] private HealthBarParentView _parentView;
        [SerializeField] private HealthBarView _healthBarView;
        
        public override void InstallBindings()
        {
            Container.BindInterfacesTo<HealthBarParentView>().FromInstance(_parentView).AsSingle();
            Container.BindInstance(_healthBarView).AsSingle();
            Container.BindInterfacesTo<HealthBarCreateHandler>().AsSingle();
            Container.BindInterfacesTo<HealthBarFactory>().AsSingle();
            Container.BindInterfacesTo<HealthBarMovement>().AsSingle();
            Container.BindInterfacesTo<HealthBarStorage>().AsSingle();
        }
    }
}