using Game.Items.Enums;
using Game.Weapons.Interfaces;
using UnityEngine;

namespace Game.Weapons.Controllers
{
    public class EmptyWeapon : IWeapon
    {
        public EItem ItemType { get; }
        public bool IsStackable { get; }
        public Sprite ItemSprite { get; }
        public int Amount { get; }
        public void SetItemAmount(int amount)
        {
            throw new System.NotImplementedException();
        }

        public void Update(float deltaTime)
        {
            
        }
    }
}