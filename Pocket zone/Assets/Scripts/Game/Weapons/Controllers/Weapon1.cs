using Game.Items.Enums;
using Game.Weapons.Interfaces;
using UnityEngine;

namespace Game.Weapons.Controllers
{
    public class Weapon1 : IWeapon
    {
        public EItem ItemType => EItem.Weapon1;
        public bool IsStackable => false;
        public Sprite ItemSprite { get; }
        public int Amount { get; }
        public void SetItemAmount(int amount)
        {
            throw new System.NotImplementedException();
        }

        public Weapon1()
        {
        }

        public void Update(float deltaTime)
        {
            
        }
    }
}