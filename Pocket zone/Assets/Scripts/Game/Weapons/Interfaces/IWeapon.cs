using Common.Mediators.Update.Interfaces;
using Game.Items.Interfaces;

namespace Game.Weapons.Interfaces
{
    public interface IWeapon : IItem, IUpdateListener
    {
        
    }
}