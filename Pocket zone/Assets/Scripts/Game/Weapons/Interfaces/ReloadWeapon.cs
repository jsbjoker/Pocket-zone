using Game.Items.Enums;
using UnityEngine;

namespace Game.Weapons.Interfaces
{
    public abstract class ReloadWeapon : IWeapon
    {
        public EItem ItemType { get; }
        public bool IsStackable { get; }
        public Sprite ItemSprite { get; }
        public int Amount { get; }
        public void SetItemAmount(int amount)
        {
            throw new System.NotImplementedException();
        }


        public void Update(float deltaTime)
        {
            throw new System.NotImplementedException();
        }
    }
}