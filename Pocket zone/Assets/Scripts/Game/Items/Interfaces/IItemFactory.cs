using Game.Items.Enums;

namespace Game.Items.Interfaces
{
    public interface IItemFactory
    {
        IItem CreateItem(EItem item, int amount);
        IItem CreateItem(EItem item);
    }
}