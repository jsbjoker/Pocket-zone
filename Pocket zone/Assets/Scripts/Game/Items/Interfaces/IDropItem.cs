using System;
using UnityEngine;

namespace Game.Items.Interfaces
{
    public interface IDropItem : IDisposable
    {
        IItem Item { get; }
        Vector3 ItemPosition { get; }
    }
}