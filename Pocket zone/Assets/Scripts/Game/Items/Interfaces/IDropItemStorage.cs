using System.Collections.Generic;

namespace Game.Items.Interfaces
{
    public interface IDropItemStorage
    {
        void AddDropItem(IDropItem dropItem);
        void RemoveDropItem(IDropItem dropItem);
        IReadOnlyList<IDropItem> DropItems { get; }
        
    }
}