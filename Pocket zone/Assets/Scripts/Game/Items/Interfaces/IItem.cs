using Game.Items.Enums;
using UnityEngine;

namespace Game.Items.Interfaces
{
    public interface IItem
    {
        EItem ItemType { get; }
        bool IsStackable { get; }
        Sprite ItemSprite { get; }
        int Amount { get; }
        void SetItemAmount(int amount);
    }
}