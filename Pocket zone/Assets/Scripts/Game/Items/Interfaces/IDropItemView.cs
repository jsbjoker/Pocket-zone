using UnityEngine;

namespace Game.Items.Interfaces
{
    public interface IDropItemView
    {
        void SetItemSprite(Sprite sprite);
        Vector3 ItemPosition { get; }
        void DestroyItem();
    }
}