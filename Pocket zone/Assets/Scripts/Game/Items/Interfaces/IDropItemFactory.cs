using System;
using Game.Items.Enums;
using UnityEngine;

namespace Game.Items.Interfaces
{
    public interface IDropItemFactory
    {
        event Action<IDropItem> OnDropItemCreate;
        void CreateDropItem(Vector3 position, EItem item, int amount);
        void CreateDropItem(Vector3 position, EItem item);
    }
}