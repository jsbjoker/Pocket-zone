using Game.Items.Enums;
using Game.Items.Interfaces;
using Game.Items.Settings;

namespace Game.Items.Controllers
{
    public class ItemFactory : IItemFactory
    {
        private readonly ItemSettings _itemSettings;

        public ItemFactory(ItemSettings itemSettings)
        {
            _itemSettings = itemSettings;
        }

        public IItem CreateItem(EItem item, int amount)
        {
            var stackableState = _itemSettings.GetItemStackable(item);
            var sprite = _itemSettings.GetItemSprite(item);
            return new Item(item, stackableState, sprite, stackableState ? amount : 1);
        }

        public IItem CreateItem(EItem item)
        {
            return CreateItem(item, 1);
        }
    }
}