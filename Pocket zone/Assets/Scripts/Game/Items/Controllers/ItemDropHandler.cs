using Common.Mediators.Initialize.Interfaces;
using Game.Items.Enums;
using Game.Items.Interfaces;
using UnityEngine;

namespace Game.Items.Controllers
{
    public class ItemDropHandler : IItemDropHandler, IInitializeListener
    {
        private readonly IDropItemFactory _dropItemFactory;

        public ItemDropHandler(IDropItemFactory dropItemFactory)
        {
            _dropItemFactory = dropItemFactory;
        }

        public void Initialize()
        {
            _dropItemFactory.CreateDropItem(new Vector3(6f, 0f, 0f), EItem.MilitaryHelmet, 1);
            _dropItemFactory.CreateDropItem(new Vector3(12f, 0f, 0f), EItem.MilitaryHelmet, 1);
            _dropItemFactory.CreateDropItem(new Vector3(18f, 0f, 0f), EItem.MilitaryHelmet, 1);
        }
    }
}