using System.Linq;
using Common.Mediators.Update.Interfaces;
using Game.Characters.Interfaces;
using Game.Inventories.Interfaces;
using Game.Items.Interfaces;
using UnityEngine;

namespace Game.Items.Controllers
{
    public class DropItemMovement : IDropItemMovement, IUpdateListener
    {
        private readonly IInventory _inventory;
        private readonly ICharacterStorage _characterStorage;
        private readonly IDropItemStorage _dropItemStorage;

        public DropItemMovement(IInventory inventory, ICharacterStorage characterStorage, IDropItemStorage dropItemStorage)
        {
            _inventory = inventory;
            _characterStorage = characterStorage;
            _dropItemStorage = dropItemStorage;
        }
        
        public void Update(float deltaTime)
        {
            if (_characterStorage.Player == null) return;
            foreach (var dropItem in _dropItemStorage.DropItems.ToList())
            {
                if (Vector3.Distance(dropItem.ItemPosition, _characterStorage.Player.Position) > 1f) return;
                _inventory.AddItem(dropItem.Item.ItemType, dropItem.Item.Amount);
                _dropItemStorage.RemoveDropItem(dropItem);
            }
        }
    }
}