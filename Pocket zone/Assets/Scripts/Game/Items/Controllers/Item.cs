using Game.Items.Enums;
using Game.Items.Interfaces;
using UnityEngine;

namespace Game.Items.Controllers
{
    public class Item : IItem
    {
        public EItem ItemType { get; }
        public bool IsStackable { get; }
        public Sprite ItemSprite { get; }
        public int Amount { get; private set; }

        public Item(EItem itemType, bool isStackable, Sprite itemSprite, int amount)
        {
            ItemType = itemType;
            IsStackable = isStackable;
            ItemSprite = itemSprite;
            Amount = amount;
        }
        
        public void SetItemAmount(int amount)
        {
            if (amount < 1) return;
            Amount = amount;
        }
    }
}