using System.Collections.Generic;
using Game.Items.Interfaces;

namespace Game.Items.Controllers
{
    public class DropItemStorage : IDropItemStorage
    {
        private readonly List<IDropItem> _dropItems = new();
        
        public void AddDropItem(IDropItem dropItem)
        {
            _dropItems.Add(dropItem);
        }

        public void RemoveDropItem(IDropItem dropItem)
        {
            _dropItems.Remove(dropItem);
            dropItem.Dispose();
        }

        public IReadOnlyList<IDropItem> DropItems => _dropItems;
    }
}