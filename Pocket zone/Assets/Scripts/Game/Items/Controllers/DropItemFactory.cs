using System;
using Game.Items.Enums;
using Game.Items.Interfaces;
using Game.Items.Views;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Game.Items.Controllers
{
    public class DropItemFactory : IDropItemFactory
    {
        private readonly IItemFactory _itemFactory;
        private readonly DropItemView _dropItem;
        private readonly IDropItemStorage _dropItemStorage;

        public event Action<IDropItem> OnDropItemCreate;

        public DropItemFactory(IItemFactory itemFactory, DropItemView dropItem, IDropItemStorage dropItemStorage)
        {
            _itemFactory = itemFactory;
            _dropItem = dropItem;
            _dropItemStorage = dropItemStorage;
        }
        
        public void CreateDropItem(Vector3 position, EItem item)
        {
            CreateDropItem(position, item, 1);
        }

        public void CreateDropItem(Vector3 position, EItem itemType, int amount)
        {
            var dropItemView = Object.Instantiate(_dropItem, position, Quaternion.identity);
            var item = _itemFactory.CreateItem(itemType, amount);
            var dropItem = new DropItem(item, dropItemView);
            _dropItemStorage.AddDropItem(dropItem);
        }
    }
}