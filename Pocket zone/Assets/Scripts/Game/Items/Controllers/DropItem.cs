using Game.Items.Interfaces;
using UnityEngine;

namespace Game.Items.Controllers
{
    public class DropItem : IDropItem
    {
        private readonly IDropItemView _dropItemView;
        public IItem Item { get; }
        public Vector3 ItemPosition => _dropItemView.ItemPosition;

        public DropItem(IItem item, IDropItemView dropItemView)
        {
            Item = item;
            _dropItemView = dropItemView;
        }

        public void Dispose()
        {
            _dropItemView.DestroyItem();
        }
    }
}