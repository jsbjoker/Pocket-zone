using Game.Items.Controllers;
using Game.Items.Settings;
using Game.Items.Views;
using UnityEngine;
using Zenject;

namespace Game.Items.Installers
{
    [CreateAssetMenu(fileName = "ItemInstaller", menuName = "Installers/Game/ItemInstaller")]
    public class ItemInstaller : ScriptableObjectInstaller
    {
        [SerializeField] private ItemSettings _itemSettings;
        [SerializeField] private DropItemView _dropItemView;

        public override void InstallBindings()
        {
            Container.BindInstance(_dropItemView).AsSingle();
            Container.BindInstance(_itemSettings).AsSingle();
            Container.BindInterfacesTo<ItemFactory>().AsSingle();
            Container.BindInterfacesTo<DropItemFactory>().AsSingle();
            Container.BindInterfacesTo<DropItemStorage>().AsSingle();
            Container.BindInterfacesTo<DropItemMovement>().AsSingle();
            Container.BindInterfacesTo<ItemDropHandler>().AsSingle();
        }
    }
}