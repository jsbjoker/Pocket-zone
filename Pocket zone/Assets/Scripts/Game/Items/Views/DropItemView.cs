using Game.Items.Interfaces;
using UnityEngine;

namespace Game.Items.Views
{
    public class DropItemView : MonoBehaviour, IDropItemView
    {
        [SerializeField] private SpriteRenderer _itemSprite;

        public void SetItemSprite(Sprite sprite) => _itemSprite.sprite = sprite;
        public Vector3 ItemPosition => transform.position;
        
        
        public void DestroyItem()
        {
            Destroy(gameObject);
        }
    }
}