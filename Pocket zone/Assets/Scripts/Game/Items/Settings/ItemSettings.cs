using System;
using Game.Items.Enums;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

namespace Game.Items.Settings
{
    [CreateAssetMenu(fileName = "ItemSettings", menuName = "Settings/Game/ItemSettings")]
    public class ItemSettings : ScriptableObject
    {
        [SerializeField] private ItemSpriteDictionary _itemSprites;
        [SerializeField] private ItemStackableDictionary _itemStackable;

        public Sprite GetItemSprite(EItem item) => _itemSprites.ContainsKey(item)
            ? _itemSprites[item]
            : throw new NullReferenceException($"Sprite for item: {item} not found");
        
        public bool GetItemStackable(EItem item) => _itemStackable.ContainsKey(item)
            ? _itemStackable[item]
            : throw new NullReferenceException($"Stackable state for item: {item} not found");
    }
    
    [Serializable]
    public class ItemSpriteDictionary : SerializableDictionaryBase<EItem, Sprite> {}
    
    [Serializable]
    public class ItemStackableDictionary : SerializableDictionaryBase<EItem, bool> {}
}